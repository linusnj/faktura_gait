<?php

return [
    'username' => env('FIKEN_USERNAME', null),
    'password' => env('FIKEN_PASSWORD', null)
];
