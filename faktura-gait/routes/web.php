<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::get('/', function () {
        return view('home');
    });

    // Home
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::post('/new/customer', [App\Http\Controllers\InvoiceController::class, 'newCustomer'])->name('newCustomer');
    Route::post('/new/fiken/invoice', [App\Http\Controllers\InvoiceController::class, 'newFikenInvoice'])->name('newFikenInvoice');
    Route::post('/new/invoice', [App\Http\Controllers\InvoiceController::class, 'createNewInvoice'])->name('createNewInvoice');

    // Invoice Panel
    Route::get('/invoice/edit/{id}', [App\Http\Controllers\InvoiceController::class, 'showInvoice'])->name('showInvoice');
    Route::post('/invoice/save/{id}', [App\Http\Controllers\InvoiceController::class, 'updateInvoice'])->name('updateInvoice');
    Route::get('/invoice/delete/{id}', [App\Http\Controllers\InvoiceController::class, 'deleteInvoice'])->name('deleteInvoice');
    Route::get('/invoice/delete/line/{id}', [App\Http\Controllers\InvoiceController::class, 'deleteInvoiceLine'])->name('deleteInvoiceLine');
    Route::post('/invoice/{invoiceId}/new/line', [App\Http\Controllers\InvoiceController::class, 'newInvoiceLine'])->name('newInvoiceLine');

    // Invoices
    Route::get('/invoices', [App\Http\Controllers\InvoicesController::class, 'index'])->name('invoices');
    Route::get('/invoice/new/', [App\Http\Controllers\NewInvoiceController::class, 'startNewInvoice'])->name('newInvoice');

    // Send invoice
    Route::get('/invoice/send', [App\Http\Controllers\InvoiceController::class, 'collectAndSendInvoices'])->name('sendInvoice');

    // Register linked user
    Route::get('/users', [App\Http\Controllers\UsersController::class, 'index'])->name('users');
    Route::post('/user/new', [App\Http\Controllers\UsersController::class, 'save'])->name('newUser');

    // Customers
    Route::get('/customers', [App\Http\Controllers\CustomerController::class, 'index'])->name('allCustomers');
    Route::get('/new/customer', [App\Http\Controllers\CustomerController::class, 'newCustomer'])->name('createNewCustomer');
    Route::get('/search/company', [App\Http\Controllers\CustomerController::class, 'searchCompany'])->name('searchCompany');


    // CompanyTools
    Route::get('/company', [App\Http\Controllers\CompanyController::class, 'index'])->name('company');
    Route::post('/company/new/user', [App\Http\Controllers\CompanyController::class, 'addUserToCompany'])->name('addUserToCompany');
    Route::post('/company/new/bankAccount', [App\Http\Controllers\CompanyController::class, 'newBankAccount'])->name('newBankAccount');
    Route::get('/company/delete/bankAccount/{id}', [App\Http\Controllers\CompanyController::class, 'deleteBankAccount'])->name('deleteBankAccount');

    // Brreg
    Route::get('/brreg', [App\Http\Controllers\BrregController::class, 'index'])->name('brreg');
    Route::get('/brreg/search', [App\Http\Controllers\BrregController::class, 'searchBrreg'])->name('searchBrreg');

    //Livewire test
    Route::view('livewire', 'pages.livewire')->name('sokSelskap');

});



