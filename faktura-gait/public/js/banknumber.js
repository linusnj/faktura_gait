/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/banknumber.js":
/*!************************************!*\
  !*** ./resources/js/banknumber.js ***!
  \************************************/
/***/ (() => {

eval("window.addEventListener('load', function (event) {\n  var banknumber = document.getElementsByClassName('banknumber');\n\n  for (var i = 0; i < banknumber.length; i++) {\n    var number = banknumber[i].innerHTML;\n    var first = number.slice(0, 4);\n    var second = number.slice(4, 6);\n    var third = number.slice(6, 11);\n    var formatedNumber = first + ' ' + second + ' ' + third;\n    console.log(formatedNumber);\n    banknumber[i].innerHTML = formatedNumber;\n  }\n});\nwindow.addEventListener('change', function (event) {\n  var height = document.getElementById('height').value;\n  var width = document.getElementById('width').value;\n  var calcTotal = height * width * 0.01;\n  var total = document.getElementById('priceTotal');\n  var formatter = new Intl.NumberFormat(\"no-NB\", {\n    style: \"currency\",\n    currency: \"NOK\"\n  });\n  total.innerHTML = formatter.format(calcTotal);\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9yZXNvdXJjZXMvanMvYmFua251bWJlci5qcz81NTM2Il0sIm5hbWVzIjpbIndpbmRvdyIsImFkZEV2ZW50TGlzdGVuZXIiLCJldmVudCIsImJhbmtudW1iZXIiLCJkb2N1bWVudCIsImdldEVsZW1lbnRzQnlDbGFzc05hbWUiLCJpIiwibGVuZ3RoIiwibnVtYmVyIiwiaW5uZXJIVE1MIiwiZmlyc3QiLCJzbGljZSIsInNlY29uZCIsInRoaXJkIiwiZm9ybWF0ZWROdW1iZXIiLCJjb25zb2xlIiwibG9nIiwiaGVpZ2h0IiwiZ2V0RWxlbWVudEJ5SWQiLCJ2YWx1ZSIsIndpZHRoIiwiY2FsY1RvdGFsIiwidG90YWwiLCJmb3JtYXR0ZXIiLCJJbnRsIiwiTnVtYmVyRm9ybWF0Iiwic3R5bGUiLCJjdXJyZW5jeSIsImZvcm1hdCJdLCJtYXBwaW5ncyI6IkFBRUFBLE1BQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsTUFBeEIsRUFBZ0MsVUFBQ0MsS0FBRCxFQUFXO0FBRXZDLE1BQU1DLFVBQVUsR0FBR0MsUUFBUSxDQUFDQyxzQkFBVCxDQUFnQyxZQUFoQyxDQUFuQjs7QUFFQSxPQUFLLElBQUlDLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdILFVBQVUsQ0FBQ0ksTUFBL0IsRUFBdUNELENBQUMsRUFBeEMsRUFBNEM7QUFFeEMsUUFBSUUsTUFBTSxHQUFHTCxVQUFVLENBQUNHLENBQUQsQ0FBVixDQUFjRyxTQUEzQjtBQUVBLFFBQU1DLEtBQUssR0FBR0YsTUFBTSxDQUFDRyxLQUFQLENBQWEsQ0FBYixFQUFlLENBQWYsQ0FBZDtBQUNBLFFBQU1DLE1BQU0sR0FBR0osTUFBTSxDQUFDRyxLQUFQLENBQWEsQ0FBYixFQUFlLENBQWYsQ0FBZjtBQUNBLFFBQU1FLEtBQUssR0FBR0wsTUFBTSxDQUFDRyxLQUFQLENBQWEsQ0FBYixFQUFlLEVBQWYsQ0FBZDtBQUVBLFFBQU1HLGNBQWMsR0FBR0osS0FBSyxHQUFHLEdBQVIsR0FBY0UsTUFBZCxHQUF1QixHQUF2QixHQUE2QkMsS0FBcEQ7QUFDQUUsSUFBQUEsT0FBTyxDQUFDQyxHQUFSLENBQVlGLGNBQVo7QUFFQVgsSUFBQUEsVUFBVSxDQUFDRyxDQUFELENBQVYsQ0FBY0csU0FBZCxHQUEwQkssY0FBMUI7QUFDSDtBQUVGLENBbEJIO0FBcUJFZCxNQUFNLENBQUNDLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLFVBQUNDLEtBQUQsRUFBVztBQUUzQyxNQUFJZSxNQUFNLEdBQUdiLFFBQVEsQ0FBQ2MsY0FBVCxDQUF3QixRQUF4QixFQUFrQ0MsS0FBL0M7QUFDQSxNQUFJQyxLQUFLLEdBQUdoQixRQUFRLENBQUNjLGNBQVQsQ0FBd0IsT0FBeEIsRUFBaUNDLEtBQTdDO0FBRUEsTUFBTUUsU0FBUyxHQUFJSixNQUFNLEdBQUdHLEtBQVYsR0FBbUIsSUFBckM7QUFFQSxNQUFNRSxLQUFLLEdBQUdsQixRQUFRLENBQUNjLGNBQVQsQ0FBd0IsWUFBeEIsQ0FBZDtBQUVBLE1BQUlLLFNBQVMsR0FBRyxJQUFJQyxJQUFJLENBQUNDLFlBQVQsQ0FBc0IsT0FBdEIsRUFBK0I7QUFDM0NDLElBQUFBLEtBQUssRUFBRSxVQURvQztBQUUzQ0MsSUFBQUEsUUFBUSxFQUFFO0FBRmlDLEdBQS9CLENBQWhCO0FBS0FMLEVBQUFBLEtBQUssQ0FBQ2IsU0FBTixHQUFrQmMsU0FBUyxDQUFDSyxNQUFWLENBQWlCUCxTQUFqQixDQUFsQjtBQUVELENBaEJEIiwic291cmNlc0NvbnRlbnQiOlsiXG5cbndpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgKGV2ZW50KSA9PiB7XG5cbiAgICBjb25zdCBiYW5rbnVtYmVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeUNsYXNzTmFtZSgnYmFua251bWJlcicpO1xuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBiYW5rbnVtYmVyLmxlbmd0aDsgaSsrKSB7XG5cbiAgICAgICAgbGV0IG51bWJlciA9IGJhbmtudW1iZXJbaV0uaW5uZXJIVE1MO1xuXG4gICAgICAgIGNvbnN0IGZpcnN0ID0gbnVtYmVyLnNsaWNlKDAsNCk7XG4gICAgICAgIGNvbnN0IHNlY29uZCA9IG51bWJlci5zbGljZSg0LDYpO1xuICAgICAgICBjb25zdCB0aGlyZCA9IG51bWJlci5zbGljZSg2LDExKTtcblxuICAgICAgICBjb25zdCBmb3JtYXRlZE51bWJlciA9IGZpcnN0ICsgJyAnICsgc2Vjb25kICsgJyAnICsgdGhpcmQ7XG4gICAgICAgIGNvbnNvbGUubG9nKGZvcm1hdGVkTnVtYmVyKTtcblxuICAgICAgICBiYW5rbnVtYmVyW2ldLmlubmVySFRNTCA9IGZvcm1hdGVkTnVtYmVyO1xuICAgIH1cblxuICB9KTtcblxuXG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCAoZXZlbnQpID0+IHtcblxuICAgIGxldCBoZWlnaHQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnaGVpZ2h0JykudmFsdWU7XG4gICAgbGV0IHdpZHRoID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3dpZHRoJykudmFsdWU7XG5cbiAgICBjb25zdCBjYWxjVG90YWwgPSAoaGVpZ2h0ICogd2lkdGgpICogMC4wMTtcblxuICAgIGNvbnN0IHRvdGFsID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3ByaWNlVG90YWwnKTtcblxuICAgIGxldCBmb3JtYXR0ZXIgPSBuZXcgSW50bC5OdW1iZXJGb3JtYXQoXCJuby1OQlwiLCB7XG4gICAgICAgIHN0eWxlOiBcImN1cnJlbmN5XCIsXG4gICAgICAgIGN1cnJlbmN5OiBcIk5PS1wiLFxuICAgIH0pO1xuXG4gICAgdG90YWwuaW5uZXJIVE1MID0gZm9ybWF0dGVyLmZvcm1hdChjYWxjVG90YWwpO1xuXG4gIH0pO1xuIl0sImZpbGUiOiIuL3Jlc291cmNlcy9qcy9iYW5rbnVtYmVyLmpzLmpzIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./resources/js/banknumber.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/js/banknumber.js"]();
/******/ 	
/******/ })()
;