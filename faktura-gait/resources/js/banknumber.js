

window.addEventListener('load', (event) => {

    const banknumber = document.getElementsByClassName('banknumber');

    for (let i = 0; i < banknumber.length; i++) {

        let number = banknumber[i].innerHTML;

        const first = number.slice(0,4);
        const second = number.slice(4,6);
        const third = number.slice(6,11);

        const formatedNumber = first + ' ' + second + ' ' + third;
        console.log(formatedNumber);

        banknumber[i].innerHTML = formatedNumber;
    }

  });


  window.addEventListener('change', (event) => {

    let height = document.getElementById('height').value;
    let width = document.getElementById('width').value;

    const calcTotal = (height * width) * 0.01;

    const total = document.getElementById('priceTotal');

    let formatter = new Intl.NumberFormat("no-NB", {
        style: "currency",
        currency: "NOK",
    });

    total.innerHTML = formatter.format(calcTotal);

  });
