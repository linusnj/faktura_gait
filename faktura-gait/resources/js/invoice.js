function curNok(price) {

    let formatter = new Intl.NumberFormat("no-NB", {
        style: "currency",
        currency: "NOK",
    });

    const newPrice = formatter.format(price);

    return newPrice;

};


function sumEachInvoiceLine () {

    const lines = document.getElementById('invoiceLines');

    lines.addEventListener('change', function() {

        calculateEachInvoiceLine();
        sumInvoiceItems();

    })
}

function calculateEachInvoiceLine() {
    const rows = document.getElementsByClassName('invoice_id');

        for (var i = 0; i < rows.length; i++) {
            let line = document.getElementsByClassName('invoice_id').item(i).value;

            let netAmount               = document.getElementById('invoice_item_netAmount' + '[' + line + ']').value;
            let quantity                = document.getElementById('invoice_item_quantity' + '[' + line + ']').value;
            let discountPercent         = document.getElementById('invoice_item_discountPercent' + '[' + line + ']').value;
            const invoiceLineTotal      = document.getElementById('invoiceLineTotal' + '[' + line + ']');
            discountPercent = discountPercent / 100;

            let grossAmount = (netAmount * quantity) * (1 - discountPercent);

            grossAmount =  Number(grossAmount);

            invoiceLineTotal.innerHTML = curNok(grossAmount);
        }
}

function curOnInput() {
    const rows = document.getElementsByClassName('invoice_id');

    for (let i = 0; i < rows.length; i++) {

        let line = document.getElementsByClassName('invoice_id').item(i).value;

        let netAmountInput = document.getElementById('invoice_item_netAmount' + '[' + line + ']').value;

        netAmountInput = curNok(netAmountInput);

    }
}

function curOnHtml() {
    const rows = document.querySelectorAll('.NOK');

    for (let i = 0; i < rows.length; i++) {

        let line = document.getElementsByClassName('invoice_id').item(i).value;

        let netAmountInput = document.getElementById('invoice_item_netAmount' + '[' + line + ']').value;

        netAmountInput = formatter.format(netAmountInput);

    }
}

function sumInvoiceItems() {

    const rows = document.getElementsByClassName('invoice_id');

    let netAmountTotal = '';

    for (let i = 0; i < rows.length; i++) {

        let line = document.getElementsByClassName('invoice_id').item(i).value; // 1, 2 ....

        let netAmount = document.getElementById('invoice_item_netAmount' + '[' + line + ']').value;
        let quantity = document.getElementById('invoice_item_quantity' + '[' + line + ']').value;
        let discountPercent = document.getElementById('invoice_item_discountPercent' + '[' + line + ']').value;
        discountPercent = Number(discountPercent);


        netAmountTotal = Number(netAmountTotal) + ((Number(netAmount) * quantity) * (1 - discountPercent / 100));

        // invoiceTotal = curNok(invoiceTotal);

    }

    let totalAmountField = document.getElementById('invoiceTotal');

    totalAmountField.innerHTML = curNok(netAmountTotal);

}


// const today = new Date();
// let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
// console.log(date);

// tomorrow.setDate(today.getDate() + 1);



function calculateDueDateOnChange () {

    const invoiceBody = document.getElementById('invoice-body');

    invoiceBody.addEventListener('change', function () {

        let issueDate   = document.getElementById('invoice_issueDate').value;
        let dueDays     = document.getElementById('invoice_dueDays').value;
        let dueDate     = document.getElementById('invoice_dueDate');

        var result = new Date(issueDate);
        result.setDate(result.getDate() + Number(dueDays));

        dueDate.value = (result.getDate() + '.' + (Number(result.getMonth()) + 1) + '.' + result.getFullYear());


    })

}

function calculateDueDate () {

    let issueDate   = document.getElementById('invoice_issueDate').value;
    let dueDays     = document.getElementById('invoice_dueDays').value;
    let dueDate     = document.getElementById('invoice_dueDate');

    var result = new Date(issueDate);
    result.setDate(result.getDate() + Number(dueDays));

    dueDate.value = (result.getDate() + '.' + (Number(result.getMonth()) + 1) + '.' + result.getFullYear());

}

function newInvoiceLine() {



}


window.onload = function exampleFunction() {
    sumEachInvoiceLine();
    calculateEachInvoiceLine();
    curOnInput();
    sumInvoiceItems();
    calculateDueDateOnChange();
    calculateDueDate();
}


