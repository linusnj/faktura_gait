<div class="row row-cols-1 row-cols-md-3 g-4">

    @foreach ($invoices as $key => $invoice)
    <div class="col">
        <div class="sm-invoice" style="cursor: pointer;" onclick="window.location='{{ route('showInvoice', ['id' => $invoice->id]) }}'">
            <div class="invoice-header">
                <div class="d-flex justify-content-between">
                    <h4 class="d-inline">Faktura: {{ $invoice->id + 1000 }}</h4>

                    @if ($invoice->status === 1)
                    <p class="badge bg-success">Sendt <span>{{ Carbon\Carbon::parse($invoice->issueDate)->format('M-d') }}</span> </p>
                    @else
                    <p class="badge bg-secondary">Sendes <span>{{ Carbon\Carbon::parse($invoice->issueDate)->format('M-d') }}</span> </p>
                    @endif

                </div>
            </div>
            <div class="invoice-body">
                <p class="d-inline">Kunde </p>
                <p class="ms-1 badge bg-primary">{{ $invoice->customer }}</p>
                <p class="card-text mt-2"><i>{{ $invoice->invoiceText }}</i></p>
                <ul class="list-group">
                    <li class="list-group-item">Sendes: <strong>{{ Carbon\Carbon::parse($invoice->issueDate)->format('d-M') }}</strong></li>
                    <li class="list-group-item">Forfall: <strong>{{ Carbon\Carbon::parse($invoice->issueDate)->addDays($invoice->dueDays)->format('d-M') }}</strong></li>
                    <li class="list-group-item active">Total: <strong class="NOK">{{ $invoice->invoiceLineLocal()->sum('netAmount') / 100 }}</strong></li>
                </ul>
            </div>
            {{-- <div class="invoice-footer">
                <a href="/invoice/edit/{{ $invoice->id }}"><button type="button" class="btn btn-outline-primary">Rediger faktura</button></a>
            </div> --}}
        </div>
    </div>
    @endforeach
</div>

