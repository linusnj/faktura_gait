<div class="form-group">
    <label for="customerInput">Kunde</label>
    <input wire:model="query" class="form-control" type="text" name="customerInput">
    @isset($customers)
    <div class="card front p-3" style="z-index: 1030;position: fixed;">
        @foreach($customers as $customer)
        <div class="card my-2 p-3">
            <h6 wire:key="{{ $customer->id }}"><strong class="badge bg-primary">{{$customer->companyname}}</strong> </h6>
            <p>{{ $customer->email }}</p>
        </div>

        @endforeach
    </div>
    @endisset
</div>
