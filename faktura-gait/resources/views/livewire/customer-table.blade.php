<div class="sm-invoice">
    <div class="my-2">
        <input type="text" wire:model="query" class="form-control" placeholder="Søk bedrift...">
    </div>
    <table class="table caption-top table-hover">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Til</th>
                <th scope="col">Send dato</th>
                <th scope="col">Forfall dager</th>
                <th scope="col">Total</th>
                <th scope="col">Status</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($customers as $customer)
            <tr>
                <th scope="row">{{ $customer->id }}</th>
                <td>{{ $customer->companyname }}</td>
                <td>{{ $customer->orgnr }}</td>
                <td>{{ $customer->email}}</td>
                <td class="NOK">{{ $customer->address }}</td>
                <td>
                    {{-- @if ($invoice->status === 1)
                    Sendt for: {{ Carbon\Carbon::parse($invoice->issueDate)->diffForHumans() }}
                    @elseif ($invoice->status === 0)
                    Ikke sendt
                    @endif --}}
                </td>
                <td>
                    {{-- <a href="{{ route('showInvoice', ['id' => $invoice->id]) }}"> <button class="btn btn-primary">Endre</button> </a> --}}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
