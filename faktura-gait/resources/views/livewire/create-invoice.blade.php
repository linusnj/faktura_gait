<form action="{{ route('createNewInvoice')}}" method="post">
    @csrf
    <div class="mt-5">
        <div class="card">
            <div class="card-header">
                <h3>Opprett ny faktura</h3>
            </div>

            <div class="card-body">
                <div class="row">
                    <div class="col-5">
                        <label for="invoice_description">Beskrivelse</label>
                        <input type="text" class="form-control" name="invoice_description">
                    </div>
                    <div class="col-3">
                        <label for="invoice_bankAccount">Konto for fakturering</label>
                        <input type="number" class="form-control" name="invoice_bankAccount" value="18224644554">
                    </div>
                    <div class="col-4">
                        <label for="invoice_customer">Navn på selskap</label>
                        <input type="text" class="form-control" name="invoice_customer" placeholder="Navnet på selskap">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Opprett ny faktura</button>
            </div>
        </div>
    </div>
</form>
