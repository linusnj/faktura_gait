<div>
    <input type="text" wire:model="query" class="form-control" placeholder="Søk etter bedrift i brreg...">

    <div>
        <table class="table table-striped table-">
            <thead>
                <th>Selskap</th>
                <th>Organisasjonsnummer</th>
                <th>Ansatte</th>
                <th>By</th>
                <th>Selskapsform</th>
            </thead>
            <tbody>
                @isset($allCompanies)
                    @foreach ($allCompanies as $company)
                        <tr>
                            <td>{{ $company->navn }}</td>
                            <td>Orgnr: {{ $company->organisasjonsnummer }}</td>
                            <td>{{ $company->antallAnsatte }}</td>
                            <td>{{ $company->forretningsadresse->poststed }}</td>
                            <td>{{ $company->organisasjonsform->kode }}</td>
                        </tr>

                    @endforeach
                @endisset
                @empty($allCompanies)
                    <tr>
                        <td colspan="5">Ingen resultater...</td>
                    </tr>
                @endempty
            </tbody>

        </table>
    </div>

</div>
