@extends('layouts.app')

@section('content')


<div class="container">

    <div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb my-3">
            <li class="breadcrumb-item">{{ Auth::user()->name }}</li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{ route('home') }}">Oversikt</a></li>
            <li class="breadcrumb-item active" aria-current="page">Fakturaoversikt</li>
            </ol>
        </nav>
    </div>

    <div class="mt-4 mb-4">
        <h3>Alle fakturaer</h3>
    </div>
    <div class="sm-invoice">
        <table class="table caption-top table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Til</th>
                    <th scope="col">Send dato</th>
                    <th scope="col">Forfall dager</th>
                    <th scope="col">Total</th>
                    <th scope="col">Status</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($invoices as $invoice)
                <tr>
                    <th scope="row">{{ $invoice->id }}</th>
                    <td>{{ $invoice->customer }}</td>
                    <td>{{ $invoice->issueDate }}</td>
                    <td>{{ $invoice->dueDays ? : 'Ikke spesifisert' }} </td>
                    <td class="NOK">{{ $invoice->sumNetAmount }}</td>
                    <td>
                        @if ($invoice->status === 1)
                        Sendt for: {{ Carbon\Carbon::parse($invoice->issueDate)->diffForHumans() }}
                        @elseif ($invoice->status === 0)
                        Ikke sendt
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('showInvoice', ['id' => $invoice->id]) }}"> <button class="btn btn-primary">Endre</button> </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>


<script src="{{ asset('js/cur.js') }}" defer></script>

@endsection
