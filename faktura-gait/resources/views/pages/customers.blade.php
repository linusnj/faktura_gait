@extends('layouts.app')

@section('content')

<div class="container">

    <div>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb my-3">
            <li class="breadcrumb-item">{{ Auth::user()->name }}</li>
            <li class="breadcrumb-item" aria-current="page"><a href="{{ route('home') }}">Oversikt</a></li>
            <li class="breadcrumb-item active" aria-current="page">Kunder</li>
            </ol>
        </nav>
    </div>

    <div class="mt-4 mb-4">
        <h3>Dine kunder </h3>
    </div>

@livewire('customer-table')

</div>

@endsection

