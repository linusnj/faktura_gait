@extends('layouts.app')

@section('content')
<div class="container">
<div class="card">
    <div class="card-header">
        <h3>Søk selskap</h3>
    </div>
    <div class="card-body">
        @livewire('companies')
        @livewire('new-customer')
    </div>

</div>

</div>


@endsection
