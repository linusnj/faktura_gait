@extends('layouts.app')

@section('content')


<div class="container">
    <div>
        <h3>Hei, {{ Auth::user()->name }}</h3>
        <h6>Du administrerer {{ Auth::user()->company->company }}</h6>
    </div>


    <div class="d-flex flex-row mt-4">
        @if (Auth::user()->role > 1)
            <div class="sm-invoice mx-2">
                <form action="{{ route('addUserToCompany') }} " method="post">
                    @csrf
                    <div class="form-group">
                        <label for="user_name">Navn</label>
                        <input type="text" class="form-control" name="user_name">
                    </div>
                    <div class="form-group mt-2">
                        <label for="user_email">E-mail</label>
                        <input type="email" class="form-control" name="user_email">
                    </div>
                    <div class="form-group mt-2">
                        <label for="user_password">Passord</label>
                        <input type="password" class="form-control" name="user_password">
                    </div>
                    <div class="form-group mt-2">
                        <label for="user_role">Rolle</label>
                        <input type="number" class="form-control" name="user_role">
                    </div>
                    <div class="mt-2">
                        <button type="submit" class="btn btn-primary">Opprett bruker</button>
                    </div>

                </form>
            </div>
        @endif

        <div class="col-sm sm-invoice mx-2">

            <div class="invoice-header">
                <div class="d-flex flex-row justify-content-between">
                    <h5>Dine kontoer</h5>
                    <button class="btn btn-primary">Ny konto</button>

                </div>
            </div>
            <div class="invoice-body">
                <table class="table table-striped table-">
                    <thead>
                        <th>Navn</th>
                        <th>Kontonummer</th>
                        <th>Notater</th>
                        <th></th>
                        <th></th>
                    </thead>
                    <tbody>
                        @isset($bankAccounts)
                            @foreach ($bankAccounts as $account)
                                <tr>
                                    <td>{{ $account->name }}</td>
                                    <td class="banknumber">{{ $account->number }}</td>
                                    <td>{{ $account->notes }}</td>
                                    <td><a href="#" class="link-primary">Endre</a></td>
                                    <td><a href="{{ route('deleteBankAccount', ['id' => $account->id]) }}" class="link-danger">Slett</a></td>
                                </tr>
                            @endforeach
                        @endisset
                        @if(!$bankAccounts)
                            <tr>
                                <td colspan="3"><strong>{{ Auth::user()->company->company }} </strong>har ingen kontoer...</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="invoice-footer">

            </div>
        </div>

        <div class="col-sm sm-invoice mx-2">
            <form action="{{ route('newBankAccount') }}" method="post">
                @csrf
                <div class="invoice-header">
                    <h5>Legg til bank konto</h5>
                </div>
                <div class="invoice-body">
                    <div class="form-group">
                        <label for="name">Navn på konto</label>
                        <input type="text" class="form-control" name="name">
                    </div>
                    <div class="form-group mt-2">
                        <label for="number">Kontonummer</label>
                        <input type="number" class="form-control" name="number">
                    </div>
                    <div class="form-group mt-2">
                        <label for="notes">Interne Notater</label>
                        <textarea class="form-control" name="notes"></textarea>
                    </div>
                </div>
                <div class="invoice-footer mt-2">
                    <button type="submit" class="btn btn-primary">Legg til konto</button>
                </div>
            </form>
        </div>
    </div>
    {{-- <div class="d-flex flex-row mt-4">
        <div class="col-sm sm-invoice mx-2">
            <div class="invoice-header">
                <h5>Regn ut Total</h5>
            </div>
            <div class="row">
                <div class="col-sm">
                    <div class="invoice-body">
                        <div class="form-group">
                            <label for="name">Type</label>
                            <input type="text" class="form-control" name="name">
                        </div>
                        <div class="form-group mt-2">
                            <label for="number">Høyde (mm)</label>
                            <input autocomplete="false" type="number" id="height" class="form-control" name="number">
                        </div>
                        <div class="form-group mt-2">
                            <label for="number">Bredde (mm)</label>
                            <input autocomplete="false" type="number" id="width" class="form-control" name="number">
                        </div>
                    </div>
                </div>
                <div class="col-sm">
                    <div class="invoice-body">
                        <h4>Total</h4>
                        <p id="priceTotal">kr 0,00</p>
                    </div>
                </div>
            </div>
            <div class="invoice-footer mt-2">
                <button type="submit" class="btn btn-primary">Legg til konto</button>
            </div>
        </div>
    </div> --}}
</div>




@endsection
