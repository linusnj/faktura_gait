@extends('layouts.app')

@section('content')
<div class="container">
    @isset($invoice)
    <form action="{{ route('updateInvoice', ['id' => $invoice->id]) }}" method="post">
    @endisset
    @empty($invoice)
    <form action="{{ route('createNewInvoice')}}" method="post">
    @endempty
        @csrf
        <div class="">

            <div class="d-flex flex-row justify-content-between">
                {{-- Breadcrumb --}}
                <div>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb my-3">
                        <li class="breadcrumb-item">{{ Auth::user()->name }}</li>
                        <li class="breadcrumb-item" aria-current="page"><a href="{{ route('home') }}">Oversikt</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Faktura: {{ $invoice->id ?? ''}}</li>
                        </ol>
                    </nav>
                </div>

                <!-- Delete invoice -->
                <div>
                    <a href="{{ route('deleteInvoice', ['id' => $invoice->id]) }}"><button type="button" class="btn btn-danger" id="deleteItem">Slett faktura</button></a>
                </div>
            </div>


            <!-- Message -->
            @if(session()->has('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <i class="fa fa-check me-2"></i>
                    {{ session()->get('message') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif

            <div class="d-flex flex-row gx-5 justify-content-between">
                <div class="me-3">
                    <div class="sm-invoice h-100">
                        <div class="invoice-header">
                            <h4>Faktura</h4>
                        </div>
                        <div class="invoice-body" id="invoice-body">
                            <div class="d-flex flex-row">
                                <div class="input-group px-2">
                                    <label for="invoice_issueDate">Dato for utsendelse</label>
                                </div>
                                <div class="input-group px-2">
                                    <label for="invoice_daysToDue">Dager til forfall</label>
                                </div>
                                <div class="input-group px-2">
                                    <label for="invoice_dueDate">Forfall</label>
                                </div>
                            </div>
                            <div class="d-flex flex-row mb-3">
                                <div class="input-group px-2" id="datepicker">
                                    <input type="date" class="form-control" placeholder="Dato for utsendelse" aria-label="Recipient's username" aria-describedby="button-addon2" id="invoice_issueDate" name="invoice_issueDate" value="{{ $invoice->issueDate ?? '' }}">
                                    <button class="btn btn-outline-primary" type="button" id="button-addon2">Utsendelse</button>
                                </div>
                                <div class="input-group px-2">
                                    <input type="number" class="form-control" placeholder="Forfall dager" aria-label="Recipient's username" aria-describedby="button-addon2" id="invoice_dueDays" name="invoice_dueDays" value="{{ $invoice->dueDays ?? ''}}">
                                </div>
                                <div class="input-group px-2">
                                    <input type="text" class="form-control" readonly placeholder="Forfall..." aria-label="Recipient's username" aria-describedby="button-addon2" id="invoice_dueDate" name="invoice_dueDate">
                                    <button class="btn btn-outline-primary" type="button" id="button-addon2">Forfall</button>
                                </div>
                            </div>

                            <div class="form-group px-2 mb-3">
                                <label for="exampleDataList" class="form-label">Kontonummer</label>
                                <select class="form-select" aria-label="Bankkonto" name="invoice_bankAccount" id="bankAccountSelect" value={{ $invoice->bankAccount ?? ''}}>
                                    @foreach (Auth::user()->company->bankAccounts as $account)
                                        <option
                                        @isset($invoice)
                                            @if ($invoice->bankAccount == $account->number) selected="selected" @endif
                                        @endisset
                                            value="{{ $account->number }}">{{$account->name}} ({{ $account->number}})
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group px-2">
                                <label for="">Kommentar</label>
                                <textarea name="invoice_invoiceText" class="form-control">{{ $invoice->invoiceText ?? ''}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="flex-fill">
                    <div class="sm-invoice h-100">
                        <div class="invoice-header d-flex flex-row justify-content-between ">
                            <h4>Kunde</h4>
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#newCustomerModal">Ny kunde</button>
                        </div>
                        <div class="invoice-body">
                            <div class="px-2 mb-3">
                                @livewire('customer-select')
                            </div>
                            <div class="d-flex flex-row">
                                <div class="input-group px-2">
                                    <label for="invoice_issueDate">Kunde</label>
                                </div>
                            </div>
                            <div class="d-flex flex-row mb-3">
                                <div class="input-group px-2">
                                    <input type="text" class="form-control" placeholder="Navn på kunde" name="invoice_customer" value="{{ $invoice->customer ?? ''}}">
                                </div>
                            </div>

                            <div class="d-flex flex-row">
                                <div class="input-group px-2">
                                    <label for="invoice_issueDate">Ordrereferanse</label>
                                </div>
                            </div>
                            <div class="input-group px-2 mb-3">
                                <input type="text" class="form-control" placeholder="Referanse (ikke påkrevd)">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="sm-invoice mt-3">
                <div class="invoice-header">
                    <h3>Fakturalinjer</h3>
                </div>
                <div class="invoice-body">
                    <div class="row mt-2 invoiceLines" id="invoiceLine">
                        <div class="row mt-2">
                            <div class="col-3">
                                <label for="invoice_item_description">Beskrivelse</label>
                            </div>
                            <div class="col-2">
                                <label for="invoice_item_netAmount">Pris</label>
                            </div>
                            <div class="col-1">
                                <label for="invoice_item_quantity">Antall</label>
                            </div>
                            <div class="col-1">
                                <label for="invoice_item_discountPercent">Rabatt</label>
                            </div>
                            <div class="col-2">
                                <label for="invoice_item_vatType">MVA</label>
                            </div>
                            <div class="col-1">
                                <label>Total</label>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-2">

                            </div>
                        </div>
                    </div>

                    <div id="invoiceLines">
                        @isset($invoiceLines)
                        @foreach ($invoiceLines as $line)
                            <div class="row mt-1 mb-4 invoiceLines" id="invoiceLine">
                                <hr/>
                                <div class="row mt-2">
                                    <div class="col-3">
                                        <input type="text" class="form-control" name="invoice_item_description[]" value="{{ $line->description }}">
                                        <input type="hidden" id="invoice_item_id" class="form-control invoice_id" name="invoice_item_id[]" value="{{ $line->id }}">
                                    </div>
                                    <div class="col-2">
                                        <input type="number" class="form-control" name="invoice_item_unitNetAmount[]" id="invoice_item_netAmount[{{ $line->id }}]" value="{{ ($line->unitNetAmount) / 100 }}">
                                    </div>
                                    <div class="col-1">
                                        <input type="number" class="form-control" name="invoice_item_quantity[]" id="invoice_item_quantity[{{ $line->id }}]" value="{{ $line->quantity }}">
                                    </div>
                                    <div class="col-1">
                                        <input type="number" class="form-control" name="invoice_item_discountPercent[]" id="invoice_item_discountPercent[{{ $line->id }}]" value="{{ $line->discountPercent }}">
                                    </div>
                                    <div class="col-2">
                                        <select type="text" class="form-select" name="invoice_item_vatType[]">
                                            <option selected="selected" value="HIGH">25%</option>
                                        </select>
                                    </div>
                                    <div class="col-2">
                                        <span class="NOK" id="invoiceLineTotal[{{ $line->id }}]">0</span>
                                    </div>
                                    <div class="col-1 mx-auto">
                                        <a href="{{ route('deleteInvoiceLine', ['id' => $line->id]) }}"><button type="button" class="btn btn-danger" id="deleteItem">Slett</button></a>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col-2">
                                        <select type="text" class="form-select" name="invoice_item_incomeAccount[]">
                                            <option value="3020" selected="selected">Tjeneste</option>
                                            <option value="3000">Vare (fysisk produkt videresalg)</option>
                                        </select>
                                    </div>
                                    <div class="col-sm">
                                        <p class="pt-2">
                                            @isset ($line->updated_at)
                                                <span class="badge bg-warning"> Oppdatert: {{ Carbon\Carbon::parse($line->updated_at)->format('d.m.Y') }}</span>
                                            @endisset
                                            @empty ($line->updated_at)
                                                Opprettet: {{ Carbon\Carbon::parse($line->created_at)->format('d.m.Y') }}
                                            @endempty
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        @endisset
                        {{-- <div class="row mt-1 mb-4 invoiceLines" id="invoiceLine">
                            <hr/>
                            <div class="row mt-2">
                                <div class="col-3">
                                    <input type="text" class="form-control" name="invoice_item_description[]">
                                    <input type="hidden" id="invoice_item_id" class="form-control invoice_id" name="invoice_item_id[]">
                                </div>
                                <div class="col-2">
                                    <input type="number" class="form-control" name="invoice_item_unitNetAmount[]" id="invoice_item_netAmount[{{ $line->id }}]">
                                </div>
                                <div class="col-1">
                                    <input type="number" class="form-control" name="invoice_item_quantity[]">
                                </div>
                                <div class="col-1">
                                    <input type="number" class="form-control" name="invoice_item_discountPercent[]">
                                </div>
                                <div class="col-2">
                                    <select type="text" class="form-select" name="invoice_item_vatType[]">
                                        <option selected="selected" value="HIGH">25%</option>
                                    </select>
                                </div>
                                <div class="col-2">
                                    <span class="">0</span>
                                <div class="col-1 mx-auto">
                                    <a href="#"><button type="button" class="btn btn-danger" id="deleteItem">Slett</button></a>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-2">
                                    <select type="text" class="form-select" name="invoice_item_incomeAccount[]">
                                        <option value="3020" selected="selected">Tjeneste</option>
                                        <option value="3000">Vare (fysisk produkt videresalg)</option>
                                    </select>
                                </div>
                            </div>
                        </div> --}}
                    </div>

                </div>
                <div class="invoice-footer">
                    <div class="d-flex bd-highlight mb-3">
                        <div class="p-2 bd-highlight">
                            <button type="submit" class="btn btn-success">Lagre faktura</button>
                        </div>
                        <div class="p-2 bd-highlight">
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#newInvoiceLineModal">Ny linje</button>
                        </div>
                        <div class="p-2 bd-highlight">
                            <button type="button" class="btn btn-info" id="newInvoiceLine">Ny innsatt linje</button>
                        </div>
                        <div class="ms-auto p-2 bd-highlight">
                            Total: <span id="invoiceTotal"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>



<!-- New invoice line modal -->
<div class="modal fade" id="newInvoiceLineModal" tabindex="-1" aria-labelledby="newInvoiceLineModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <form action="{{ route('newInvoiceLine', ['invoiceId' => $invoiceId ?? '-1']) }}" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ny faktura linje</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row mt-2">
                        <div class="col-3">
                            <input type="text" class="form-control" name="new_invoice_item_description" placeholder="Beskrivelse...">
                        </div>
                        <div class="col-2">
                            <input type="number" class="form-control" name="new_invoice_item_unitNetAmount" id="new_invoice_item_netAmount" placeholder="Pris...">
                        </div>
                        <div class="col-2">
                            <input type="number" class="form-control" name="new_invoice_item_quantity" id="new_invoice_item_quantity" placeholder="Ant">
                        </div>
                        <div class="col-2">
                            <input type="number" class="form-control" name="new_invoice_item_discountPercent" id="new_invoice_item_discountPercent" placeholder="Rabatt">
                        </div>
                        <div class="col-2">
                            <select type="text" class="form-select" name="new_invoice_item_vatType">
                                <option selected="selected" value="HIGH">25% MVA</option>
                            </select>
                        </div>
                        {{-- <div class="col-1">
                            <span class="NOK" id="new_invoiceLineTotal">0</span>
                        </div> --}}
                    </div>
                    <div class="row mt-2">
                        <div class="col-2">
                            <select type="text" class="form-select" name="new_invoice_item_incomeAccount">
                                <option value="3020" selected="selected">Tjeneste</option>
                                <option value="3000">Vare (fysisk produkt videresalg)</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Lukk</button>
                    <button type="submitt" class="btn btn-primary">Legg til linje</button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
