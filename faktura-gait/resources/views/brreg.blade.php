@extends('layouts.app')

@section('content')

<div class="container">
    <h3>Søk i brreg</h3>
    <div class="sm-invoice">
        <div>
            <form action="{{ route('searchBrreg') }}" method="get">
                <label for="brreg_search">Søk i brreg</label>
                <input name="brreg_search" class="form-control dropdown-toggle" type="text" id="dropdownMenu2" data-bs-toggle="dropdown" aria-expanded="false" placeholder="Søk etter bedrift...">
            </form>
        </div>
        <div class="sm-invoice" id="searchComponent">
            <div class="sm-header">

            </div>
        </div>
    </div>

    <div class="sm-invoice mt-4">
        <table class="table table-striped table">
            <thead>
                <th>Navn</th>
                <th>Orgnr</th>
                <th>Ansatte</th>
                <th>Registrert</th>
                <th>Addresse</th>
            </thead>
            <tbody>
                @isset ($companyList)
                    @foreach ($companyList as $company)
                    <tr>
                        <td>{{ $company->navn }}</td>
                        <td>{{ $company->organisasjonsnummer }}</td>
                        <td>{{ $company->antallAnsatte }}</td>
                        <td>{{ $company->registreringsdatoEnhetsregisteret }}</td>
                        <td>{{ $company->forretningsadresse->adresse[0] }}</td>
                    </tr>


                    @endforeach
                @endisset
                @empty ($companyList)
                    <tr>
                        <td colspan="6"><h4>Ingen resultater...</h4></td>
                    </tr>
                @endempty
            </tbody>
        </table>
    </div>
</div>

@endsection
