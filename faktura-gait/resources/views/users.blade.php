@extends('layouts.app')

@section('content')


<div class="d-flex flex-row w-50 mx-auto">
    <div class="input-group">
        <input class="form-control" type="text" id="user_name" placeholder="Fullt navnt" name="user_name">
        <input class="form-control" type="email" id="user_email" placeholder="Epost" name="user_email">
        <input class="form-control" type="password" id="user_password" placeholder="Passord" name="user_password">
        <button onclick='proccess()' class="btn btn-primary">Lag bruker</button>
    </div>
</div>

@foreach ($users as $key => $user)
    <br>
    {{ $user->name }}
    <br>
    {{ $user->email }}
    <br>
    {{ Carbon\Carbon::parse($user->created_at)->diffForHumans() }}
    <br>

@endforeach


<script defer>

    function proccess(){
       var laravelToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

       axios.post('{{ route('newUser') }}', {
        user_name: document.getElementById('user_name').value,
        user_email: document.getElementById('user_email').value,
        user_password: document.getElementById('user_password').value,
      });
    };
</script>


@endsection
