@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="">

            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif

            {{-- Breadcrumb --}}
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb my-3">
                  <li class="breadcrumb-item">{{ Auth::user()->name }}</li>
                  <li class="breadcrumb-item active" aria-current="page">Oversikt</li>
                </ol>
              </nav>

            <div class="mt-4 mb-4">
                <h3>Siste fakturaer</h3>
            </div>

            @livewire('show-invoices')
            @livewire('create-invoice')


            <div class="card mt-5">
                <div class="card-header">
                    <h3>Lag ny faktura</h3>
                </div>
                <form action="{{ route('newFikenInvoice') }}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="row">
                            <div class="col-5">
                                <label for="invoice_description">Beskrivelse</label>
                                <input type="text" class="form-control" name="invoice_description">
                            </div>
                            <div class="col-3">
                                <label for="invoice_bankAccount">Konto for fakturering</label>
                                <input type="number" class="form-control" name="invoice_bankAccount" value="18224644554">
                            </div>
                            <div class="col-4">
                                <label for="invoice_customer">Navn på selskap</label>
                                <input type="text" class="form-control" name="invoice_customer" placeholder="Navnet på selskap">
                            </div>
                        </div>


                        <div class="row mt-5" id="invoiceLine">
                            <hr/>
                            <div class="row mt-2">
                                <div class="col-3">
                                    <label for="invoice_item_description">Beskrivelse</label>
                                    <input type="text" class="form-control" name="invoice_item_description">
                                </div>
                                <div class="col-2">
                                    <label for="invoice_item_netAmount">Pris</label>
                                    <input type="number" class="form-control" name="invoice_item_netAmount" id="invoice_item_netAmount">
                                </div>
                                <div class="col-1">
                                    <label for="invoice_item_quantity">Antall</label>
                                    <input type="number" value="1" class="form-control" name="invoice_item_quantity" id="invoice_item_quantity">
                                </div>
                                <div class="col-1">
                                    <label for="invoice_item_discount">Rabatt %</label>
                                    <input type="number" class="form-control" name="invoice_item_discountPercent" id="invoice_item_discountPercent">
                                </div>
                                <div class="col-2">
                                    <label for="invoice_item_vatType">MVA</label>
                                    <select type="text" class="form-select" name="invoice_item_vatType">
                                        <option selected="selected" value="HIGH">25%</option>
                                    </select>
                                </div>
                                <div class="col-1">
                                    <label>Total</label>
                                    <br>
                                    <span id="invoiceLineTotal">0</span>
                                </div>
                                <div class="col-1 mx-auto">
                                    <label>Slett</label>
                                    <button type="button" class="btn btn-danger" id="deleteItem">Slett</button>
                                </div>
                                <div class="col-1">
                                    <label>Ny linje</label>
                                    <button type="button" class="btn btn-primary" id="addItem">Ny</button>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-2">
                                    <select type="text" class="form-select" name="invoice_item_incomeAccount">
                                        <option value="3020" selected="selected">Tjeneste</option>
                                        <option value="3000">Vare (fysisk produkt videresalg)</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Lagre faktura</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/cur.js') }}" defer></script>

{{-- 'netAmount'         => 150000,
     'vatType'           => 'HIGH',
     'vatAmount'         => 37500,
     'grossAmount'       => 187500,
     'description'       => 'Testens tester du tester du product',
     'incomeAccount'     => '3030', --}}
@endsection
