<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('invoice_id');
            $table->foreign('invoice_id')
                ->references('id')->on('invoices')
                ->onDelete('cascade');
            $table->integer('unitNetAmount');
            $table->integer('netAmount');
            $table->integer('vatAmount');
            $table->integer('grossAmount');
            $table->string('description');
            $table->string('comment')->nullable();
            $table->string('vatType');
            $table->string('productUrl')->nullable();
            $table->string('incomeAccount');
            $table->integer('discountPercent')->default(0)->nullable();
            $table->integer('quantity');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_items');
    }
}
