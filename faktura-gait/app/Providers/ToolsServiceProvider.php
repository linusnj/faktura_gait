<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Tools\InvoiceHandler;

class ToolsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('InvoiceHandler', function() {
            return new InvoiceHandler(
                config('fiken.username'),
                config('fiken.password')
            );
        });
    }
}
