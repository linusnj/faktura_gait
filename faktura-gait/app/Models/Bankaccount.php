<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bankaccount extends Model
{
    use HasFactory;

    protected $table = 'bankaccounts';

    protected $fillable = [
        'number',
        'name',
        'notes'
    ];

    // public function invoiceLineLocal() {
    //     return $this->hasMany(InvoiceLineLocal::class, 'invoice_id', 'id');
    // }

    public function company() {
        return $this->belongsTo(InvoiceLocal::class, 'id', 'company_id');
    }
}
