<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceLineLocal extends Model
{
    use HasFactory;

    protected $fillable = [
        'unitNetAmount',
        'netAmount',
        'vatAmount',
        'grossAmount',
        'description',
        'comment',
        'vatType',
        'incomeAccount',
        'discountPercent',
        'quantity',
    ];

    protected $appends = [
        'grossAmount'
    ];

    protected $table = 'invoice_items';

    public function invoiceLocal()
    {
        return $this->belongsTo(InvoiceLocal::class, 'invoice_id', 'id');
    }

    // public function getGrossAmountAttribute()
    // {
    //     return ($this->quantity * $this->netAmount) * ($this->vatType / 100);
    // }
}
