<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceLocal extends Model
{
    use HasFactory;


    protected $table = 'invoices';

    protected $fillable = [
        'user_id',
        'invoiceText',
        'issueDate',
        'dueDays',
        'customer',
        'bankAccount',
        'status'
    ];

    public function invoiceLineLocal()
    {
        return $this->hasMany(InvoiceLineLocal::class, 'invoice_id', 'id');
    }
}
