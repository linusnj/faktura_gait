<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;


    protected $table = 'companies';

    public function invoice() {

        return $this->hasMany(InvoiceLocal::class, 'company_id', 'id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'company_id', 'id');
    }

    public function invoices() {
        return $this->hasMany(InvoiceLocal::class, 'company_id', 'id');
    }

    public function bankAccounts()
    {
        return $this->hasMany(Bankaccount::class, 'company_id', 'id');
    }
    public function customers()
    {
        return $this->hasMany(Customer::class, 'company_id', 'id');
    }
}
