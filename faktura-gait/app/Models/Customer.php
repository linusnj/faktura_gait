<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $table = 'customers';

    protected $fillable = [
        'companyname',
        'orgnr',
        'email',
        'phone',
        'address',
        'state',
        'city',
        'zip',
        'country'
    ];

    public function company() {

        return $this->belongsTo(Customer::class, 'id', 'company_id');

    }
}
