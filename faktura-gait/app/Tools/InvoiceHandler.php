<?php

namespace App\Tools;

use Carbon\Carbon;

use App\Models\InvoiceLocal;
use App\Models\InvoiceLineLocal;

use audunru\FikenClient\FikenClient;
use audunru\FikenClient\Models\Contact;
use audunru\FikenClient\Models\Invoice;
use audunru\FikenClient\Models\InvoiceLine;

class InvoiceHandler
{
    private $username;
    private $password;

    function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function create($invoiceLocal)
    {
        // Get Fiken sender client
        $client = new FikenClient();
        $client->authenticate($this->username, $this->password);                // LOCAL ONLY
        $company = $client->setCompany('123456789');                            // ORG.NR TO COMPANY SENDING INVOICE

        // Create a new invoice object
        $invoice = new Invoice(['issueDate' => Carbon::now()->toDateString(), 'dueDate' => Carbon::now()->addDays(14)->toDateString()]);

        // Define customer
        $customer = $company->contacts()->firstWhere('name', $invoiceLocal->customer);
        $invoice->setCustomer($customer);

        // Find a bank account and set it on the invoice
        $bankAccount = $company->bankAccounts()->firstWhere('bankAccountNumber', $invoiceLocal->bankAccount);
        $invoice->setBankAccount($bankAccount);

        $invoice->invoiceText = $invoiceLocal->invoiceText;

        $sendableInvoiceLines = InvoiceLineLocal::where('invoice_id', '=', $invoiceLocal->id)->count();

        // Check whether the invoice has any invoice lines.
        if ($sendableInvoiceLines > 0) {

            // Get all invoice lines since they exist
            $invoiceLines = InvoiceLineLocal::where('invoice_id', '=', $invoiceLocal->id)->get();

            // Loops through invoice lines
            foreach ($invoiceLines as $key => $line) {

                $line = new InvoiceLine([
                    'unitNetAmount'     => $line->unitNetAmount,
                    'netAmount'         => $line->netAmount,
                    'vatType'           => $line->vatType,
                    'vatAmount'         => $line->vatAmount,
                    'quantity'          => $line->quantity,
                    'grossAmount'       => $line->grossAmount,
                    'description'       => $line->description,
                    'incomeAccount'     => $line->incomeAccount,
                    'discountPercent'   => $line->discountPercent,
                ]);

                $invoice->add($line);
            }

            $invoiceLocal->update([
                'status' => 1
            ]);

            $saved = $company->add($invoice);
        }

        return $sendableInvoiceLines;
    }
}

?>
