<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use App\Models\InvoiceLocal;
use App\Models\InvoiceLineLocal;
use audunru\FikenClient\FikenClient;
use audunru\FikenClient\Models\Contact;
use audunru\FikenClient\Models\Invoice;
use audunru\FikenClient\Models\InvoiceLine;
use Carbon\Carbon;

class createFikenInvoices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fiken:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Capture invoices and send to Fiken';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $now = Carbon::now()->format('Y-m-d'); // = '2022-02-08';
        $sendableInvoiceCount = InvoiceLocal::where('issueDate', '=', $now)->count();

        // Check whether there is invoices that's sendable today.
        if ($sendableInvoiceCount > 0) {

            $invoicesToSend = InvoiceLocal::where('issueDate', '=', $now)->where('status', '=', 0)->get();

            // Loops through all invoices that are sendable with DATE TODAY
            foreach ($invoicesToSend as $key => $invoiceLocal) {
                $resCount = resolve('InvoiceHandler')->create($invoiceLocal);

                print('Send til: ' . $invoiceLocal->customer . '. Med ' . $resCount . ' fakturalinjer' . PHP_EOL);
            }

        } else {
            dd('Ingen fakturer');
        }

        return 0;
    }
}
