<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Bankaccount;
use App\Models\Company;

class CompanyController extends Controller
{

    public function index() {

        // dd(date("Y/m/d G:i:s"));

        $bankAccounts = Auth::user()->company->bankAccounts;

        return view('pages.company')->with('bankAccounts', $bankAccounts);

    }

    public function addUserToCompany(Request $req) {

        // Check if user is administrator
        if (Auth::user()->role === 2) {

            $company_id = Auth::user()->company_id;

            $user = new User();
            $user->company_id       = $company_id;
            $user->name             = $req->user_name;
            $user->email            = $req->user_email;
            $user->password         = Hash::make($req->user_password);
            $user->role             = $req->user_role;
            $user->save();

            return back();

        } else {
            return back();
        }
    }

    public function newBankAccount(Request $req) {

        $company = Auth::user()->company->id;

        $account = new Bankaccount;
        $account->company_id    = $company;
        $account->name          = $req->name;
        $account->number        = $req->number;
        $account->notes         = $req->notes;
        $account->save();

        return redirect()->back();

    }

    public function deleteBankAccount($id) {

        $companyId = Auth::user()->company->id; // Company ID
        $bankAccountCompanyId = Bankaccount::select('company_id')->where('id', $id)->first();

        if ($bankAccountCompanyId->company_id === $companyId) {

            $account = Bankaccount::find($id);
            $account->delete();

            return redirect()->back();

        } else {

            return redirect()->back();

        }
    }
}
