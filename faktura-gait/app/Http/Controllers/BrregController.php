<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\InvoiceLocal;
use App\Models\InvoiceLineLocal;
use GuzzleHttp\Client as GuzzleClient;

class BrregController extends Controller
{
    public function index() {

        return view('brreg');

    }

    public function searchBrreg(Request $request) {

        $companyName = $request->brreg_search;

        try {
            $client = new GuzzleClient();
            $req = $client->request('GET', 'https://data.brreg.no/enhetsregisteret/api/enheter?navn=' . $companyName);

            $result = json_decode($req->getBody());
            $companys = $result->_embedded->enheter;

            $companyList = array_slice($companys, 0, 9);

            //dd($companyList);


        } catch (RequestException $re) {
            dd('Feil');
        }

        return view('brreg')
            ->with('companyList', $companyList);


    }
}
