<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\InvoiceLocal;
use App\Models\InvoiceLineLocal;

class InvoicesController extends Controller
{
    public function index() {

        $invoices = InvoiceLocal::where('company_id', Auth::user()->company_id)->orderBy('id', 'desc')->get()->map(function($invoiceLocal) {
            $invoiceLocal->sumNetAmount = $invoiceLocal->invoiceLineLocal()->sum('netAmount') / 100;

            return $invoiceLocal;
        });

        return view('pages.invoices')
            ->with('invoices', $invoices);

    }
}
