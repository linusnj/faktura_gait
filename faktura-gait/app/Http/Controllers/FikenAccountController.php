<?php

namespace App\Http\Controllers;

use App\Http\Requests\Storefiken_accountRequest;
use App\Http\Requests\Updatefiken_accountRequest;
use App\Models\fiken_account;

class FikenAccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Storefiken_accountRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Storefiken_accountRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\fiken_account  $fiken_account
     * @return \Illuminate\Http\Response
     */
    public function show(fiken_account $fiken_account)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\fiken_account  $fiken_account
     * @return \Illuminate\Http\Response
     */
    public function edit(fiken_account $fiken_account)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Updatefiken_accountRequest  $request
     * @param  \App\Models\fiken_account  $fiken_account
     * @return \Illuminate\Http\Response
     */
    public function update(Updatefiken_accountRequest $request, fiken_account $fiken_account)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\fiken_account  $fiken_account
     * @return \Illuminate\Http\Response
     */
    public function destroy(fiken_account $fiken_account)
    {
        //
    }
}
