<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index() {

        $users = User::get();

        return view('users')->with('users', $users);
    }

    public function save(request $request) {


        // $name = $request->input('user_name');
        // $email = $request->input('user_email');
        // $password = $request->input('user_password');
        // echo "tu nombre es $name y tu apellido es $surname";

        $password = $request->input('user_password');

        $data = new User();
        $data->name = $request->input('user_name');
        $data->email = $request->input('user_email');
        $data->password = Hash::make($password);
        $data->save();

    }
}
