<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\InvoiceLocal;

class NewInvoiceController extends Controller
{
    public function index() {

        $invoice = null;
        return view('pages.invoice');
    }

    public function startNewInvoice() {

        $next = InvoiceLocal::max('id') + 1;

        $invoice = new InvoiceLocal();
        $invoice->id = $next;
        $invoice->company_id = Auth::user()->company->id;
        $invoice->invoiceText = '';
        $invoice->issueDate =  null;
        $invoice->dueDays =  null;
        $invoice->customer = '';
        $invoice->bankAccount = '';
        $invoice->save();

        return redirect()->route('showInvoice', ['id' => $next]);

    }
}
