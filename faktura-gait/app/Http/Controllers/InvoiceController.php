<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreinvoiceRequest;
use App\Http\Requests\UpdateinvoiceRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\InvoiceLocal;
use App\Models\InvoiceLineLocal;
use audunru\FikenClient\FikenClient;
use audunru\FikenClient\Models\Contact;
use audunru\FikenClient\Models\Invoice;
use audunru\FikenClient\Models\InvoiceLine;
use Carbon\Carbon;

class InvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    public function newCustomer(Request $request) {
        $client = new FikenClient();
        $client->authenticate('linus@nordixgroup.no', 'pleat-traps-accompany');

        $company = $client->setCompany('123456789');

        $customer = new Contact([
            'name'                      => $request->customer_name,
            'email'                     => $request->customer_email,
            'organizationIdentifier'    => $request->customer_organizationIdentifier,
            'customer'                  => true
        ]);

        // $saved is a new FikenCustomer object
        $saved = $company->add($customer);

        return back();
    }

    public function newFikenInvoice(Request $request) {
        $client = new FikenClient();
        $client->authenticate('linus@nordixgroup.no', 'pleat-traps-accompany');

        $company = $client->setCompany('123456789');



        // Create a new invoice object
        $invoice = new Invoice(['issueDate' => Carbon::now()->toDateString(), 'dueDate' => Carbon::now()->addDays(14)->toDateString()]);



        // Find an existing customer of this company and set it on the invoice
        $customer = $company->contacts()->firstWhere('name', $request->invoice_customer);
        $invoice->setCustomer($customer);


        // Find a bank account and set it on the invoice
        $bankAccount = $company->bankAccounts()->firstWhere('bankAccountNumber', $request->invoice_bankAccount);
        $invoice->setBankAccount($bankAccount);

        // Set invoice text
        $invoice->invoiceText = $request->invoice_description;

        // Find a product
        // $product = $company->products()->firstWhere('name', 'Demovare');


        // If statement for calculating total for quantity. Else calculate 1.
        if($request->invoice_item_quantity > 0) {

            $netAmount = ($request->invoice_item_netAmount * 100)* $request->invoice_item_quantity;
            $vatAmount = $netAmount * 0.25;
            $grossAmount = $netAmount + $vatAmount;

        } else {

            $netAmount = $request->invoice_item_netAmount * 100;
            $vatAmount = $netAmount * 0.25;
            $grossAmount = $netAmount + $vatAmount;

        }



            // Create a new invoice line
            $line = new InvoiceLine([
                'netAmount'         => $netAmount,
                'vatType'           => $request->invoice_item_vatType,
                'vatAmount'         => $vatAmount,
                'quantity'          => $request->invoice_item_quantity,
                'grossAmount'       => $grossAmount,
                'description'       => $request->invoice_item_description,
                'incomeAccount'     => $request->invoice_item_incomeAccount,
                'discountPercent'   => (int) $request->invoice_item_discountPercent,
                ]
            );
            $line1 = new InvoiceLine([
                'netAmount'         => $netAmount,
                'vatType'           => $request->invoice_item_vatType,
                'vatAmount'         => $vatAmount,
                'quantity'          => $request->invoice_item_quantity,
                'grossAmount'       => $grossAmount,
                'description'       => $request->invoice_item_description,
                'incomeAccount'     => $request->invoice_item_incomeAccount,
                'discountPercent'   => (int) $request->invoice_item_discountPercent,
                ]
            );
            $line2 = new InvoiceLine([
                'netAmount'         => $netAmount,
                'vatType'           => $request->invoice_item_vatType,
                'vatAmount'         => $vatAmount,
                'quantity'          => $request->invoice_item_quantity,
                'grossAmount'       => $grossAmount,
                'description'       => $request->invoice_item_description,
                'incomeAccount'     => $request->invoice_item_incomeAccount,
                'discountPercent'   => (int) $request->invoice_item_discountPercent,
                ]
            );
            $line3 = new InvoiceLine([
                'netAmount'         => $netAmount,
                'vatType'           => $request->invoice_item_vatType,
                'vatAmount'         => $vatAmount,
                'quantity'          => $request->invoice_item_quantity,
                'grossAmount'       => $grossAmount,
                'description'       => $request->invoice_item_description,
                'incomeAccount'     => $request->invoice_item_incomeAccount,
                'discountPercent'   => (int) $request->invoice_item_discountPercent,
                ]
            );





        // Add the invoice line to the invoice
        $invoice->add($line);
        $invoice->add($line1);
        $invoice->add($line2);
        $invoice->add($line3);

        dd($invoice);

        // dd($invoice);
        // Add the invoice to the company
        $saved = $company->add($invoice);
        // $saved is a new Invoice object

        return back();
    }

    public function createNewInvoice(Request $req) {
        $data = new InvoiceLocal();

        $data->company_id = Auth::user()->id;
        $data->invoiceText = $req->invoice_description;
        $data->issueDate =  null;
        $data->dueDate =  null;
        $data->customer = $req->invoice_customer;
        $data->bankAccount = $req->invoice_bankAccount;

        $data->save();

        return back();
    }

    public function newInvoiceLine(Request $req, $invoiceId) {

        $unitNetAmount = $req->new_invoice_item_unitNetAmount * 100;

        if($req->new_invoice_item_quantity > 1) {
            $quantity = $req->new_invoice_item_quantity;
            $netAmount = $unitNetAmount * $quantity;
            $vatAmount = $netAmount * 0.25;
            $grossAmount = ($netAmount + $vatAmount);// * $quantity;
        } else {
            $quantity = $req->new_invoice_item_quantity;
            $netAmount = $unitNetAmount;
            $vatAmount = $netAmount * 0.25;
            $grossAmount = $netAmount + $vatAmount;
        }

        $data = new InvoiceLineLocal();

        $data->invoice_id = 1;

        $data->invoice_id = $invoiceId;
        $data->unitNetAmount = $unitNetAmount;
        $data->netAmount = $netAmount;
        $data->vatAmount = $vatAmount;
        $data->grossAmount = $grossAmount;
        $data->quantity = $quantity;
        $data->description = $req->new_invoice_item_description;
        $data->incomeAccount = $req->new_invoice_item_incomeAccount;
        $data->vatType = $req->new_invoice_item_vatType;
        $data->discountPercent = $req->new_invoice_item_discountPercent;

        $data->save();

        return back();


    }

    public function showInvoice($id) {


        $invoice = InvoiceLocal::where('id', '=', $id)->first();
        $invoiceLines = InvoiceLineLocal::where('invoice_id', '=', $invoice->id)->get();
        $invoiceLineSum = $invoice->invoiceLineLocal()->sum('netAmount') / 100;
        $invoiceId = $invoice->id;

        $this->checkIfUserHasAccess($invoice->company_id);

        return view('pages.invoice')
            ->with('invoice', $invoice)
            ->with('invoiceLines', $invoiceLines)
            ->with('invoiceLineSum', $invoiceLineSum)
            ->with('invoiceId', $invoiceId);

    }

    public function updateInvoice(Request $req, $id) {

        // dd($req->invoice_item_id);

        // $validated = $req->validate([
        //     'invoice_issueDate' => 'required|max:255',
        //     'invoice_dueDate' => 'required|max:255',
        //     'invoice_bankAccount' => 'required|max:255',
        //     'invoice_invoiceText' => 'required|max:255',
        //     'invoice_customer' => 'required|max:255',
        // ]);

        InvoiceLocal::where('id', '=', $id)->update([
            'issueDate'     => $req->invoice_issueDate,
            'dueDays'       => $req->invoice_dueDays,
            'bankAccount'   => $req->invoice_bankAccount,
            'invoiceText'   => $req->invoice_invoiceText,
            'customer'      => $req->invoice_customer,
        ]);

        if($req->invoice_item_id > 0) {

            foreach($req->invoice_item_id as $key => $id) {

                $unitNetAmount = $req->invoice_item_unitNetAmount[$key] * 100;


                if($req->invoice_item_quantity[$key] > 1) {
                    $quantity = $req->invoice_item_quantity[$key];
                    $netAmount = $unitNetAmount * $quantity;
                    $vatAmount = $netAmount * 0.25;
                    $grossAmount = ($netAmount + $vatAmount);// * $quantity;
                } else {
                    $netAmount = $unitNetAmount;
                    $vatAmount = $netAmount * 0.25;
                    $grossAmount = $netAmount + $vatAmount;
                }

                InvoiceLineLocal::where('id', '=', $id)->update([
                    'description'       => $req->invoice_item_description[$key],
                    'unitNetAmount'     => $unitNetAmount,
                    'netAmount'         => $netAmount,
                    'vatAmount'         => $vatAmount,
                    'grossAmount'       => $grossAmount,
                    'quantity'          => $req->invoice_item_quantity[$key],
                    'discountPercent'   => $req->invoice_item_discountPercent[$key],
                    'vatType'           => $req->invoice_item_vatType[$key],
                    'incomeAccount'     => $req->invoice_item_incomeAccount[$key]
                ]);
            }
        }

        return redirect()->back()->with('message', 'Fakturaen er oppdatert');

    }

    public function deleteInvoice(Request $req, $id) {

        $invoiceLocal = InvoiceLocal::find($id);

        if (!$invoiceLocal || $invoiceLocal->company_id != auth()->user()->id) {
            return abort(401);
        }

        $invoiceLocal->delete();

        return redirect()->route('home');

    }

    public function deleteInvoiceLine(Request $req, $id) {

        $invoiceLine = InvoiceLineLocal::find($id);

        if (!$invoiceLine || $invoiceLine->InvoiceLocal->company_id != auth()->user()->id) {
            return abort(401);
        }

        $invoiceLine->delete();

        return back();

    }

    public function addNewInvoiceLine($invoiceId) {

        $invoiceLine = new InvoiceLineLocal();


    }


    // SENDING
    public function collectAndSendInvoices() {

        $now = Carbon::now()->format('Y-m-d'); // = '2022-02-08';
        $sendableInvoiceCount = InvoiceLocal::where('issueDate', '=', $now)->count();

        // Check whether there is invoices that's sendable today.
        if ($sendableInvoiceCount > 0) {

            $invoicesToSend = InvoiceLocal::where('issueDate', '=', $now)->get();
            //$invoicesToSend = InvoiceLocal::where('issueDate', '=', $now)->where('status', '=', 0)->get();



            // Loops through all invoices that are sendable with DATE TODAY
            foreach ($invoicesToSend as $key => $invoiceLocal) {
                $resCount = resolve('InvoiceHandler')->create($invoiceLocal);

                var_dump($resCount);
            }

        } else {
            dd('Ingen fakturer');
        }
    }



    private function checkIfUserHasAccess($iuid) {

        if (Auth::user()->company_id !== $iuid) {
            abort(401);
        }

    }

    private function calculateGrossAmount($net, $vatType, $quantity) {

        $netTotal = $net * $quantity;
        $vatTotal  = $netTotal * $vatType;
        $grossTotal = $netTotal * $vatTotal;

        return [
            $netTotal,
            $vatTotal,
            $grossTotal
        ];


    }

}
