<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use audunru\FikenClient\FikenClient;
use App\Models\InvoiceLocal;
use App\Models\Company;
use App\Models\User;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $userCompany = Auth::user()->company()->pluck('id');
        // $companyInvoices = InvoiceLocal::where('company_id', $userCompany)->get();

        // dump($companyInvoices);
        return view('home');
    }
}
