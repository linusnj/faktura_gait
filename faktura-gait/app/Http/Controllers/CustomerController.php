<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\InvoiceLocal;
use App\Models\InvoiceLineLocal;
use GuzzleHttp\Client as GuzzleClient;
use audunru\FikenClient\FikenClient;
use audunru\FikenClient\Models\Contact;
use audunru\FikenClient\Models\Invoice;
use audunru\FikenClient\Models\InvoiceLine;


class CustomerController extends Controller
{

    public function index() {



        // $client = new FikenClient();
        // $client->authenticate(config('fiken.username'), config('fiken.password'));
        // $company = $client->setCompany('123456789');

        // $customer = $company->contacts()->firstWhere('name', 'Gait AS');

        // dd($customer);

        return view('pages.customers');

    }

    public function searchCompany(Request $request) {

        $companyName = 'Gait';

        try {
            $client = new GuzzleClient();
            $req = $client->request('GET', 'https://data.brreg.no/enhetsregisteret/api/enheter?navn=' . $companyName);

            $result = json_decode($req->getBody());
            $companys = $result->_embedded->enheter;

            $companyList = array_slice($companys, 0, 3);

            dump($companyList);


        } catch (RequestException $re) {
            dd('Feil');
        }

        return view('home')
            ->with('companyList', $companyList);


    }
}
