<?php

namespace App\Http\Controllers;

use App\Http\Requests\Storeinvoice_itemsRequest;
use App\Http\Requests\Updateinvoice_itemsRequest;
use App\Models\InvoiceLineLocal;

class InvoiceItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Storeinvoice_itemsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Storeinvoice_itemsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\invoice_items  $invoice_items
     * @return \Illuminate\Http\Response
     */
    public function show(invoice_items $invoice_items)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\invoice_items  $invoice_items
     * @return \Illuminate\Http\Response
     */
    public function edit(invoice_items $invoice_items)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Updateinvoice_itemsRequest  $request
     * @param  \App\Models\invoice_items  $invoice_items
     * @return \Illuminate\Http\Response
     */
    public function update(Updateinvoice_itemsRequest $request, invoice_items $invoice_items)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\invoice_items  $invoice_items
     * @return \Illuminate\Http\Response
     */
    public function destroy(invoice_items $invoice_items)
    {
        //
    }
}
