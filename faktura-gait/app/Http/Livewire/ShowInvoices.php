<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\InvoiceLocal;
use App\Models\InvoiceLineLocal;
use App\Models\Company;
use App\Models\User;

class ShowInvoices extends Component
{
    public function render()
    {
        $userCompany = Auth::user()->company()->pluck('id');

        $invoices = InvoiceLocal::orderBy('id', 'desc')->where('company_id', '=', Auth::user()->company_id)->take(3)->get()->sortByDesc('id');
        $invoiceId = InvoiceLocal::where('company_id', '=', Auth::user()->id)->pluck('id')->toArray();

        $sumNetInvoiceItems = InvoiceLineLocal::where('invoice_id', '=', $invoiceId)->pluck('netAmount');


        $sumGrossInvoiceItems = InvoiceLineLocal::where('invoice_id', '=', $invoiceId)->sum('grossAmount')/100;
        $invoices_total = InvoiceLocal::where('company_id', '=', Auth::user()->id)->get();

        return view('livewire.show-invoices')
            ->with('invoices', $invoices);
    }


}
