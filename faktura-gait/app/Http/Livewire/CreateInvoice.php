<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CreateInvoice extends Component
{
    public function render()
    {
        return view('livewire.create-invoice');
    }
}
