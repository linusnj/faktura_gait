<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Company;
use GuzzleHttp\Client as GuzzleClient;

class Companies extends Component
{

    public $query = '';

    public function render()
    {
        if (strlen($this->query) > 2) {

                $client = new GuzzleClient();
                $req = $client->request('GET', 'https://data.brreg.no/enhetsregisteret/api/enheter?navn=' . $this->query);

                $result = json_decode($req->getBody());
                $companys = $result->_embedded->enheter;

                $companyList = array_slice($companys, 0, 9);

                //dd($companyList);

                // $allCompanies = Company::where('company', 'like', '%'.$this->query.'%')->get();

        } else {

            $companyList = null;

        }



        return view('livewire.companies')->with('allCompanies', $companyList);
    }
}
