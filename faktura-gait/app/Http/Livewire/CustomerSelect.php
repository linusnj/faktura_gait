<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Company;
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;

class CustomerSelect extends Component
{
    public $query = '';

    public function render()
    {
        if(strlen($this->query) > 0) {
            // $customers = Auth::user()->company->customers->where('companyname', 'like', '%'.$this->query.'%')->get();
            $customers = Customer::where('company_id', Auth::user()->company->id)
                ->where('companyname', 'like', '%' . $this->query . '%')
                ->get();
            } else {
                $customers = null;
            }

        return view('livewire.customer-select')->with('customers', $customers);

    }
}

