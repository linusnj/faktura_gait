<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\Company;
use App\Models\User;
use App\Models\Customer;


class CustomerTable extends Component
{

    public $query = '';

    public function render()
    {

        $customers = Customer::where('company_id', Auth::user()->company_id)
            ->where('companyname', 'like', '%' . $this->query . '%')
            ->get();

        return view('livewire.customer-table')
            ->with('customers', $customers);
    }
}
